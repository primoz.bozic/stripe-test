import React, {Component} from 'react';
import {CardElement, CardNumberElement, CardExpiryElement, CardCVCElement, injectStripe} from 'react-stripe-elements';
import axios from "axios";

class CheckoutForm extends Component {
  constructor(props) {
	super(props);
	this.state = {complete: false, error: false};
	this.submit = this.submit.bind(this);
	this.reset = this.reset.bind(this);
  }

  async submit(ev) {

		let {token, error} = await this.props.stripe.createToken({name: "Primoz Bozic"});

		if (error) {
			this.setState({complete: false, error: error});
			return;
		}
		let response = await axios({
			method: 'post',
			url: 'https://localhost/api/subscriptions/charge',
			data: {token: token.id},
			headers: {token: "9aec8bb78987b17adae30d7fe70cfb15"}
		});
		console.log(response.code)
		if (response.result.code == 0) {
			this.setState({complete: true});
		}
  }
  reset() {
	  console.log(this);
	this.setState({complete: false, error: false});
  }
  render() {
	  if (this.state.error) {
		  return (
			<div>
				<h1 className="error">{this.state.error.message}</h1>
				<button onClick={this.reset}>Back</button>
			</div>
			);
	}
	if (this.state.complete) return <h1>Purchase Complete</h1>;
    return (
	   <div className="checkout">
		<p>Would you like to complete the purchase?</p>
		<label>Number:</label>
		<CardNumberElement /> <br />
		<label>Expiry:</label>
		<CardExpiryElement /> <br />
		<label>CVC:</label>
		<CardCVCElement /> <br />
		<button onClick={this.submit}>Send</button>
	  </div>
    );
  }
}

export default injectStripe(CheckoutForm);