import React, {Component} from 'react';
import {CardElement, CardNumberElement, CardExpiryElement, CardCVCElement, injectStripe} from 'react-stripe-elements';
import axios from "axios";

class Products extends Component {
  constructor(props) {
	super(props);
	this.state = {complete: false, error: false};
	this.submit = this.submit.bind(this);
	this.reset = this.reset.bind(this);
  }

  async submit(ev) {

	let {token, error} = await this.props.stripe.createToken({name: "Primoz Bozic"});

	if (error) {
		this.setState({complete: false, error: error});
		return;
	}
	console.log("tkn", token);
	console.log("tknerr", error);
	let response = await fetch("http://localhost/subscriptions/charge", {
	  method: "POST",
	  headers: {"Content-Type": "text/plain"},
	  body: token.id
	});

	if (response.ok) this.setState({complete: true});
  }
  reset() {
	  console.log(this);
	this.setState({complete: false, error: false});
  }
  render() {
	  if (this.state.error) {
		  return (
			<div>
				<h1 className="error">{this.state.error.message}</h1>
				<button onClick={this.reset}>Back</button>
			</div>
			);
	}
	if (this.state.complete) return <h1>Purchase Complete</h1>;
    return (
	   <div className="checkout">
		<p>Would you like to complete the purchase?</p>
		<label>Number:</label>
		<CardNumberElement /> <br />
		<label>Expiry:</label>
		<CardExpiryElement /> <br />
		<label>CVC:</label>
		<CardCVCElement /> <br />
		<button onClick={this.submit}>Send</button>
	  </div>
    );
  }
}

export default injectStripe(CheckoutForm);